var num : number = 5
var i : number
var factorial = 1

for (i = num;i >= 1;i --) {
  factorial *= 1
}

console.log(factorial)

var j : any
var n : any = "a b c"

for (j in n) {
  console.log(n[j])
}

let someArray = [1, "string", false]
for (let entry of someArray) {
  console.log(entry)
}

let list = [4, 5, 6]
list.forEach((val, idx, array) => {
  console.log(val);
  console.log(idx);
  console.log(array)
})

while(num >= 1) {
  factorial = factorial * num
  num--
}
console.log(factorial)

do {
  console.log(num)
  num--
} while(n >= 0)

var i : number = 1
while (i <= 10) {
  if ( i % 5 == 0) {
    console.log('0-10');
    break
  }
  i++
}
