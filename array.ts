var sites:string[]
sites = ["Google", "Runoob", "Taobao"]
console.log(sites[0])
console.log(sites[1])

var arr_names:number[] = new Array(4)
for (var i = 0; i < arr_names.length; i++) {
  arr_names[i] = i * 2
  console.log(arr_names[i])
}

var arr:number[] = [12, 13]
var[x, y] = arr
console.log(x)
console.log(y)

var j:any
var nums:number[] = [1001, 1002, 1003, 1004]
for(j in nums) {
  console.log(nums[j])
}

var multi:number[][] = [[1, 2, 3],[23, 45, 67]]
console.log(multi[0][0])
console.log(multi[1][1])

var sites1:string[] = new Array("Google", "Runoob", "Taobao", "Facebook")
function disp(arr_sites:string[]) {
  for (var i = 0; i < arr_sites.length; i++) {
    console.log(arr_sites[i])
  }
}
disp(sites1)

function disp1():string[]{
  return new Array("Google", "Runoob", "Taobao", "Facebook")
}
var sites2:string[] = disp1()
for (var ii in sites2) {
  console.log(sites2[ii])
}

sites2.forEach(function (value){
  console.log(value)
})