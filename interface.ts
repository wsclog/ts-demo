interface IPersion {
  firstName:string,
  lastName:string,
  sayHi:() => string
}

var customer:IPersion = {
  firstName: "Tome",
  lastName: "Hanks",
  sayHi: ():string => {
    return "Hi there"
  }
}
console.log("Customer")
console.log(customer.firstName)
console.log(customer.lastName)
console.log(customer.sayHi())

var employee:IPersion = {
  firstName: "Jim",
  lastName: "Blakes",
  sayHi: ():string => {
    return "Hello!"
  }
}
console.log("employee")
console.log(employee.firstName)
console.log(employee.lastName)
console.log(employee.sayHi())


interface RunOptions {
  program:string,
  conmmandline:string[]|string|(() => string)
}

var options:RunOptions = {
  program: "test1",
  conmmandline: "Hello"
}
console.log(options.conmmandline)

options = {
  program: "test2",
  conmmandline: ["Hello", "World"]
}
console.log(options.conmmandline[0])
console.log(options.conmmandline[1])

options = {
  program: "test3",
  conmmandline: () => {
      return "**Hello World**"
  }
}
var fn:any = options.conmmandline
console.log(fn());


interface Person {
  age:number
}

interface Musician extends Person {
  instrument:string
}

var drummer = <Musician>{}
drummer.age = 27
drummer.instrument = "Drums"
console.log(drummer.age)
console.log(drummer.instrument)