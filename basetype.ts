let x : [string, number]
x = ['Runoob', 1]
// x = [1, 'Runoob']
console.log(x[0])

enum Color {Red, Green, Blue}
let c : Color = Color.Blue
console.log(c)

function hello() : void {
  console.log("Hello Runoob")
}

hello()

let xx : any = 1
console.log(xx)

xx = 'I am who I am'
console.log(xx)

xx = false
console.log(xx)

let arrayList : any[] = [1, false, 'fine']
console.log(arrayList)
arrayList[1] = 100
console.log(arrayList)

let y : number | null | undefined
y = 1
console.log(y)
y = undefined
console.log(y)
y = null
console.log(y)

