var num = 5;
var i;
var factorial = 1;
for (i = num; i >= 1; i--) {
    factorial *= 1;
}
console.log(factorial);
var j;
var n = "a b c";
for (j in n) {
    console.log(n[j]);
}
var someArray = [1, "string", false];
for (var _i = 0, someArray_1 = someArray; _i < someArray_1.length; _i++) {
    var entry = someArray_1[_i];
    console.log(entry);
}
var list = [4, 5, 6];
list.forEach(function (val, idx, array) {
    console.log(val);
    console.log(idx);
    console.log(array);
});
while (num >= 1) {
    factorial = factorial * num;
    num--;
}
console.log(factorial);
do {
    console.log(num);
    num--;
} while (n >= 0);
var i = 1;
while (i <= 10) {
    if (i % 5 == 0) {
        console.log('0-10');
        break;
    }
    i++;
}
