var num : number = 5
if (num > 0) {
  console.log('shuzi是正数')
}

if (num % 2 == 0) {
  console.log('oushu')
} else {
  console.log('jishu')
}

if (num > 0) {
  console.log('zhengshu')
} else if (num < 0) {
  console.log('fushu')
} else {
  console.log('no zhengshu no fushu')
}

var grade : string = 'A'
switch(grade) {
  case 'A': {
    console.log('you')
    break
  }
  case 'B': {
    console.log('liang')
    break
  }
  case 'C': {
    console.log('jige')
    break
  }
  case 'D': {
    console.log('no jige')
    break
  }
  default: {
    console.log('no')
    break
  }
}