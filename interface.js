var customer = {
    firstName: "Tome",
    lastName: "Hanks",
    sayHi: function () {
        return "Hi there";
    }
};
console.log("Customer");
console.log(customer.firstName);
console.log(customer.lastName);
console.log(customer.sayHi());
var employee = {
    firstName: "Jim",
    lastName: "Blakes",
    sayHi: function () {
        return "Hello!";
    }
};
console.log("employee");
console.log(employee.firstName);
console.log(employee.lastName);
console.log(employee.sayHi());
var options = {
    program: "test1",
    conmmandline: "Hello"
};
console.log(options.conmmandline);
options = {
    program: "test2",
    conmmandline: ["Hello", "World"]
};
console.log(options.conmmandline[0]);
console.log(options.conmmandline[1]);
options = {
    program: "test3",
    conmmandline: function () {
        return "**Hello World**";
    }
};
var fn = options.conmmandline;
console.log(fn());
var drummer = {};
drummer.age = 27;
drummer.instrument = "Drums";
console.log(drummer.age);
console.log(drummer.instrument);
