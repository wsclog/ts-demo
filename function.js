function test() {
    console.log("function");
}
test();
function greet() {
    return "Hello World";
}
function caller() {
    var msg = greet();
    console.log(msg);
}
caller();
function add(x, y) {
    return x + y;
}
console.log(add(1, 2));
function buildName(firstName, lastName) {
    return firstName + lastName;
}
console.log(buildName("Bob", "Adams"));
function calculate_discount(price, rate) {
    if (rate === void 0) { rate = 0.50; }
    var discount = price * rate;
    console.log(discount);
}
calculate_discount(1000);
calculate_discount(1000, 0.30);
function addNumbers() {
    var nums = [];
    for (var _i = 0; _i < arguments.length; _i++) {
        nums[_i] = arguments[_i];
    }
    var i;
    var sum = 0;
    for (i = 0; i < nums.length; i++) {
        sum = sum + nums[i];
    }
    console.log(sum);
}
addNumbers(1, 2, 3);
var msg = function () {
    return "hello world";
};
console.log(msg());
var res = function (a, b) {
    return a * b;
};
console.log(res(12, 2));
// (function () {
//   var x = "hello!"
//   console.log(x)
// })()
var myFunction = new Function("a", "b", "return a * b");
var x = myFunction(3, 4);
console.log(x);
function factorial(number) {
    if (number <= 0) {
        return 1;
    }
    else {
        return (number * factorial(number - 1));
    }
}
console.log(factorial(6));
var foo1 = function (x) { return 10 + x; };
console.log(foo1(100));
var foo2 = function (x) {
    x = 10 + x;
    console.log(x);
};
foo2(100);
var func = function (x) {
    if (typeof x == "number") {
        console.log("shuzi");
    }
    else if (typeof x == "string") {
        console.log("zifuchuan");
    }
};
func(120);
func("TOM");
function disp(x, y) {
    console.log(x);
    console.log(y);
}
disp("abc");
disp(1, "xyz");
