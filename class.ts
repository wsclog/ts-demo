class Person {}

class Car {
  engine:string
  constructor(engine:string){
    this.engine = engine
  }
  disp():void {
    console.log(this.engine)
  }
}

var obj = new Car("XXSY1")
console.log(obj.engine)
obj.disp()

class Shape {
  Area:number
  constructor(a:number){
    this.Area = a
  }
}

class Circle extends Shape {
  disp():void {
    console.log(this.Area)
  }
}
var obj1 = new Circle(223)
obj1.disp()

class Root {
  str:string
}

class Child extends Root {}
class Leaf extends Child {}

var obj2 = new Leaf()
obj2.str = "hello"
console.log(obj2.str)

class PrinterClass {
  doPrint():void {
    console.log("doPrint")
  }
}

class StringPrinter extends PrinterClass {
  doPrint():void {
    super.doPrint()
    console.log("child doPrint")
  }
}
var obj3 = new StringPrinter()
obj3.doPrint()

class StaticMem {
  static num:number
  static disp():void {
    console.log(StaticMem.num)
  }
}
StaticMem.num = 12
StaticMem.disp()

var obj4 = new Person()
var isPerson = obj4 instanceof Person
console.log(isPerson)

class Encapsulate {
  str1:string = "hello"
  private str2:string = "word"
}

var obj5 = new Encapsulate()
console.log(obj5.str1)
// console.log(obj5.str2)

interface ILoan {
  interest:number
}

class AgriLoan implements ILoan {
  interest:number
  rebate:number
  constructor(interest:number, rebate:number) {
    this.interest = interest
    this.rebate = rebate
  }
}
var obj6 = new AgriLoan(10, 1)
console.log(obj6.interest + obj6.rebate)

