function test () {
  console.log("function")
}
test()

function greet() : string {
  return "Hello World"
}

function caller () {
  var msg = greet()
  console.log(msg)
}
caller()

function add(x : number, y : number) : number {
  return x + y
}
console.log(add(1, 2))

function buildName (firstName : string, lastName : string) {
  return firstName + lastName
}
console.log(buildName("Bob", "Adams"))

function calculate_discount(price : number, rate : number = 0.50) {
  var discount = price * rate
  console.log(discount)
}
calculate_discount(1000)
calculate_discount(1000, 0.30)

function addNumbers(...nums : number[]){
  var i
  var sum : number = 0
  for (i = 0;i < nums.length;i++) {
    sum = sum + nums[i]
  }
  console.log(sum)
}
addNumbers(1, 2, 3)

var msg = function () {
  return "hello world"
}
console.log(msg())

var res = function(a:number, b:number) {
  return a * b
}

console.log(res(12, 2))

// (function () {
//   var x = "hello!"
//   console.log(x)
// })()

var myFunction = new Function("a", "b", "return a * b")
var x = myFunction(3, 4)
console.log(x)

function factorial(number) {
  if (number <= 0) {
    return 1
  } else {
    return (number * factorial(number -1))
  }
}
console.log(factorial(6))

var foo1 = (x:number) => 10 + x
console.log(foo1(100))

var foo2 = (x:number) => {
  x = 10 + x
  console.log(x)
}
foo2(100)

var func = x => {
  if (typeof x == "number"){
    console.log("shuzi")
  } else if (typeof x == "string") {
    console.log("zifuchuan")
  }
}
func(120)
func("TOM")

function disp(s1:string):void
function disp(n1:number, s1:number):void
function disp(x:any, y?:any):void{
  console.log(x)
  console.log(y)
}
disp("abc")
// disp(1, "xyz")






