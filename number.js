console.log(Number.MAX_VALUE);
console.log(Number.MIN_VALUE);
console.log(Number.NEGATIVE_INFINITY);
console.log(Number.POSITIVE_INFINITY);
var month = 0;
if (month <= 0 || month > 12) {
    month = Number.NaN;
    console.log(month);
}
else {
    console.log("else");
}
function employee(id, name) {
    this.id = id;
    this.name = name;
}
var emp = new employee(123, "admin");
employee.prototype.email = "admin@runoob.com";
console.log(emp.id);
console.log(emp.name);
console.log(emp.email);
